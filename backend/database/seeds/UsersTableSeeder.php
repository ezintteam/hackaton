<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$pe9LQiaDwvw124ixfue.1.bs/GB8nwqV4Y4s8Z.4b196prZXKRJt.',
                'role_id' => 1,
                'balance' => 1432,
                'created_at' => '2019-11-04 23:06:26',
                'updated_at' => '2019-11-05 00:38:59',
            ),
            1 => 
            array (
                'id' => 2,
                'first_name' => 'Marko',
                'last_name' => 'Marković',
                'email' => 'marko@gmail.com',
                'password' => '$2y$10$nMlk3JIxDdV/Hb3.HGyjROV0HIW0KshqgCYeWdkpKa0KUSVeLIhlC',
                'role_id' => 1,
                'balance' => 8235,
                'created_at' => '2019-11-04 23:12:04',
                'updated_at' => '2019-11-05 00:38:59',
            ),
        ));
        
        
    }
}