<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('items')->delete();
        
        \DB::table('items')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'Piva',
                'description' => 'Karlovacko 0.5',
                'price' => 100,
                'event_id' => 1,
                'created_at' => '2019-11-04 22:26:49',
                'updated_at' => '2019-11-04 22:36:17',
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'Piva',
                'description' => 'Ožujsko 0.5',
                'price' => 120,
                'event_id' => 1,
                'created_at' => '2019-11-04 23:10:03',
                'updated_at' => '2019-11-04 23:10:03',
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'Piva',
                'description' => 'Laško 0.5',
                'price' => 120,
                'event_id' => 2,
                'created_at' => '2019-11-04 23:10:28',
                'updated_at' => '2019-11-04 23:10:28',
            ),
            2 => 
            array (
                'id' => 5,
                'name' => 'Sok',
                'description' => 'Coca Cola',
                'price' => 70,
                'event_id' => 2,
                'created_at' => '2019-11-04 23:10:28',
                'updated_at' => '2019-11-04 23:10:28',
            ),
            2 => 
            array (
                'id' => 6,
                'name' => 'Sok',
                'description' => 'Fanta',
                'price' => 70,
                'event_id' => 2,
                'created_at' => '2019-11-04 23:10:28',
                'updated_at' => '2019-11-04 23:10:28',
            ),
            2 => 
            array (
                'id' => 7,
                'name' => 'Sok',
                'description' => 'Pago',
                'price' => 70,
                'event_id' => 2,
                'created_at' => '2019-11-04 23:10:28',
                'updated_at' => '2019-11-04 23:10:28',
            ),
            2 => 
            array (
                'id' => 8,
                'name' => 'Ćevapi',
                'description' => 'petica',
                'price' => 70,
                'event_id' => 2,
                'created_at' => '2019-11-04 23:10:28',
                'updated_at' => '2019-11-04 23:10:28',
            ),
            2 => 
            array (
                'id' => 9,
                'name' => 'Ćevapi',
                'description' => 'desetka',
                'price' => 70,
                'event_id' => 2,
                'created_at' => '2019-11-04 23:10:28',
                'updated_at' => '2019-11-04 23:10:28',
            ),
        ));
        
        
    }
}