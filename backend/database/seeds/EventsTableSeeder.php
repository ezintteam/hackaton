<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('events')->delete();
        
        \DB::table('events')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Summerfest 2',
                'description' => 'neki ludi koncert galamdzije',
                'date_from' => '2019-11-04 21:57:19',
                'date_to' => '2019-11-04 21:57:19',
                'price' => 333,
                'latitude' => '43.34303300',
                'longitude' => '17.80789400',
                'created_at' => '2019-11-04 22:26:42',
                'updated_at' => '2019-11-04 22:26:42',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Rolling Stones',
                'description' => 'Jedan od ponajboljih bendova svih vremena ponovno u Mostaru',
                'date_from' => '2019-11-04 21:57:19',
                'date_to' => '2019-11-04 21:57:29',
                'price' => 200,
                'latitude' => '43.34303300',
                'longitude' => '17.80789400',
                'created_at' => '2019-11-04 23:08:56',
                'updated_at' => '2019-11-04 23:08:56',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'WHF',
                'description' => 'Svake godine ponovno sa vama',
                'date_from' => '2019-11-21 21:57:19',
                'date_to' => '2019-11-24 21:57:29',
                'price' => 100,
                'latitude' => '43.34303300',
                'longitude' => '17.80789400',
                'created_at' => '2019-11-04 23:08:56',
                'updated_at' => '2019-11-04 23:08:56',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Mostar Film Festival',
                'description' => 'Odabrani i nagrađivani filmovi samo za vas',
                'date_from' => '2019-12-21 21:57:19',
                'date_to' => '2019-12-24 21:57:29',
                'price' => 100,
                'latitude' => '43.34303300',
                'longitude' => '17.80789400',
                'created_at' => '2019-11-04 23:08:56',
                'updated_at' => '2019-11-04 23:08:56',
            ),
            3 => 
            array (
                'id' => 5,
                'name' => 'Festival hrane',
                'description' => 'Najbolji ukusi i slastice u gradu',
                'date_from' => '2019-12-05 21:57:19',
                'date_to' => '2019-12-05 21:57:29',
                'price' => 100,
                'latitude' => '43.34303300',
                'longitude' => '17.80789400',
                'created_at' => '2019-11-04 23:08:56',
                'updated_at' => '2019-11-04 23:08:56',
            ),
        ));
        
        
    }
}