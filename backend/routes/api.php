<?php

// Auth
Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\RegisterController@register');
Route::middleware('auth:api')->post('logout', 'Auth\LoginController@logout');
Route::middleware('auth:api')->get('/users/logged', 'Auth\LoginController@getUser');

Route::middleware('auth:api')->post('events/{event}/buy', 'EventController@buyEvent');

Route::middleware('auth:api')->post('items/{item}/buy', 'ItemController@buyItem');
Route::middleware('auth:api')->resource('items', 'ItemController');

Route::get('events', 'EventController@index');
Route::get('places', 'PlaceController@index');
Route::middleware('auth:api')->get('transactions', 'TransactionController@index');
Route::middleware('auth:api')->resource('events', 'EventController')->except('index');
Route::middleware('auth:api')->resource('places', 'PlaceController')->except('index');
Route::middleware('auth:api')->post('buy_tokens', 'TransactionController@buyTokens');

