<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\DB;

class UpdateBalanceForUsersListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    // Jebem vam nazive !
    public function handle($laravelEvent)
    {
        DB::transaction(function () use ($laravelEvent) {
            $laravelEvent->sender->balance -= $laravelEvent->amount;
            $laravelEvent->sender->save();
            $laravelEvent->receiver->balance += $laravelEvent->amount;
            $laravelEvent->receiver->save();
        });
    }
}
