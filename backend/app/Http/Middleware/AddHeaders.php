<?php

namespace App\Http\Middleware;
use Closure;

class AddHeaders
{
    public function handle($request, Closure $next)
    {
        $bearer_token = $request->cookie('ezint_token');
        if ( ! $bearer_token) {
            return $next($request);
        }

        $request->headers->set('Authorization', "Bearer {$bearer_token}");

        return $next($request);
    }
}
