<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Response;
use GuzzleHttp\Exception\BadResponseException;
use Cookie;

class LoginController extends Controller
{

    public function login(Request $request) {

        $http = new \GuzzleHttp\Client;

        try {
            $response = $http->post(route('passport.token'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password,
                    'scope' => ''
                ],
            ]);


            $resp = json_decode($response->getBody());

            Cookie::queue('ezint_token', // Name
                $resp->access_token, // Value
                864000, // 10 days
                null, // path
                null, // domain
                false, // secure
                true); // HttpOnly

            return $response->getBody();

        } catch (BadResponseException $e) {
            if ($e->getCode() === 400) {
                return response()->json('Invalid Request. Please enter a username or a password.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
            }
            return response()->json('Something went wrong on the server.' . $e, $e->getCode());
        }

    }

    public function logout(Request $request) {

        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Thank you.',
        ], 200);

    }

    public function getUser(Request $request)
    {
        $user = $request->user();
        return $user;
    }

}
