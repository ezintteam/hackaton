<?php

namespace App\Http\Controllers;

use App\Events\UserBoughtTokensEvent;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Kada user kupuje tokene za pravi novac
    public function buyTokens(Request $request)
    {
        $this->validate(request(), [
            'amount' => 'required|integer'
        ]);
        $user = $request->user();
        $amount = $request->amount;
        // lafo neka logika sa Stripeom ili čime već pa kad se to procesuira upisemo mu tokene u bazu
        $transaction = Transaction::create([
            'receiver_id' => $user->id,
            'amount' => $request->amount
        ]);
        event(new UserBoughtTokensEvent($transaction));

        return $request->user();
    }

    public function index(Request $request)
    {
        return $request->user()->allTransactions();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
