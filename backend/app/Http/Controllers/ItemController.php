<?php

namespace App\Http\Controllers;

use App\Events\UserSpentTokensEvent;
use App\Models\Item;
use App\Models\Purchase;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use App\Models\Image;

class ItemController extends Controller
{
    public function index()
    {
        return Item::with('images')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'price' => 'required|integer',
            'event_id' => 'required|integer'
        ]);

        $item = Item::create(request([
            'name',
            'description',
            'price',
            'event_id'
        ]));

        $images = $request->file('images');
        Storage::disk('public')->makeDirectory('items');

        foreach ($images as $image) {
            $finalFile = time() . "." . $image->getClientOriginalExtension();
            $path = Storage::disk('public')->putFileAs('items', $image, $finalFile);
            Image::create([
                'path' => $path,
                'item_id' => $item->id
            ]);
        }

        return $item;
    }

    public function show(Item $item)
    {
        return $item->with('images');
    }

    public function edit(Item $item)
    {
        //
    }

    public function update(Request $request, Item $item)
    {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255'
        ]);

        $item->update(request([
            'name',
            'description'
        ]));

        return response($item, 200);
    }

    public function destroy(Item $item)
    {
        $item->delete();
        return response('Success', 204);
    }

    public function buyItem(Request $request, Item $item)
    {
        $user = $request->user();
        if ($user->balance < $item->price) {
            return Response::json('Nemate dovoljno tokena na računu', 400);
        }
        $purchaseTransaction = DB::transaction(function () use ($request, $item, $user) {
            $transaction = Transaction::create([
                'sender_id' => $user->id,
                'receiver_id' => env('ORGANISATOR_ID'),
                'amount' => $item->price,
            ]);
            $purchase = Purchase::create([
                'transaction_id' => $transaction->id,
                'item_id' => $item->id
            ]);
            return $transaction;
        });
        event(new UserSpentTokensEvent($purchaseTransaction));

        return $purchaseTransaction;
    }
}
