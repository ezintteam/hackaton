<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Image;
use Illuminate\Support\Facades\Storage;
use App\Models\Place;

class PlaceController extends Controller
{

    public function index()
    {
        return Place::with('images')->get();
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);

        $place = Place::create(request([
            'name',
            'description',
            'latitude',
            'longitude'
        ]));

        $images = $request->file('images');
        Storage::disk('public')->makeDirectory('places');

        foreach ($images as $image) {
            $finalFile = time() . "." . $image->getClientOriginalExtension();
            $path = Storage::disk('public')->putFileAs('places', $image, $finalFile);
            Image::create([
                'path' => $path,
                'place_id' => $place->id
            ]);
        }

        return $place;
    }

    public function show(Place $place)
    {
        return $place->with('images');
    }


    public function update(Request $request, Place $place)
    {
        //
    }

    public function destroy(Place $place)
    {
        $place->delete();
        return response('Success', 204);
    }

}
