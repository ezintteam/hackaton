<?php

namespace App\Http\Controllers;

use App\Events\UserSpentTokensEvent;
use App\Models\Event;
use App\Models\Image;
use App\Models\Purchase;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{
    public function index()
    {
        $sortBy = request()->query('sort_by');
        $this->validate(request(), [
            'sort_by' => 'in:new,popular',
        ]);
        if ($sortBy == 'new')
            return Event::with('items', 'images')->orderBy('created_at', 'desc')->get();
        if ($sortBy == 'popular')
            return Event::with('items', 'images')->withCount('purchases')->orderBy('purchases_count', 'desc')->get();

        return Event::with('items', 'images')->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'date_from' => 'required',
            'date_to' => 'required',
            'price' => 'required|integer',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);

        $event = Event::create(request([
            'name',
            'description',
            'date_from',
            'date_to',
            'price',
            'latitude',
            'longitude'
        ]));

        $images = $request->file('images');
        Storage::disk('public')->makeDirectory('events');

        foreach ($images as $image) {
            $finalFile = time() . "." . $image->getClientOriginalExtension();
            $path = Storage::disk('public')->putFileAs('events', $image, $finalFile);
            Image::create([
                'path' => $path,
                'event_id' => $event->id
            ]);
        }

        return $event;
    }

    public function show(Event $event)
    {
        return $event->with('items', 'images')->get();
    }

    public function edit(Event $event)
    {
        //
    }

    public function update(Request $request, Event $event)
    {
        //
    }

    public function destroy(Event $event)
    {
        $event->delete();
        return response('Success', 204);
    }

    public function buyEvent(Request $request, Event $event)
    {
        $user = $request->user();
        if ($user->balance < $event->price) {
            return Response::json('Nemate dovoljno tokena na računu', 400);
        }
        $purchaseTransaction = DB::transaction(function () use ($request, $event, $user) {
            $transaction = Transaction::create([
                'sender_id' => $user->id,
                'receiver_id' => env('ORGANISATOR_ID'),
                'amount' => $event->price,
            ]);
            $purchase = Purchase::create([
                'transaction_id' => $transaction->id,
                'event_id' => $event->id
            ]);
            return $transaction;
        });
        event(new UserSpentTokensEvent($purchaseTransaction));

        return $purchaseTransaction;
    }
}
