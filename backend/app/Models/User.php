<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    protected $hidden = [
        'password'
    ];

//    public function sender()
//    {
//        return $this->hasOne(Transaction::class, 'sender_id');
//    }
//
//    public function receiver()
//    {
//        return $this->hasOne(Transaction::class, 'receiver_id');
//    }

    public function sentTransactions()
    {
        return $this->hasMany(Transaction::class, 'sender_id')->with('sender', 'receiver');
    }

    public function receivedTransactions()
    {
        return $this->hasMany(Transaction::class, 'receiver_id')->with('sender', 'receiver');
    }

    public function allTransactions() {
        return $this->sentTransactions->merge($this->receivedTransactions);
    }
}
