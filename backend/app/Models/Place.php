<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{

    protected $fillable = ['name', 'description', 'latitude', 'longitude'];

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
