<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'description', 'price', 'event_id'];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function purchase()
    {
        return $this->hasOne(Purchase::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
