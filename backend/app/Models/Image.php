<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'event_id', 'item_id', 'place_id', 'path'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }
}
