<?php

namespace App\Providers;

use App\Events\UserBoughtTokensEvent;
use App\Events\UserSpentTokensEvent;
use App\Listeners\AddBoughtTokensToUserListener;
use App\Listeners\UpdateBalanceForUsersListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserBoughtTokensEvent::class => [
            AddBoughtTokensToUserListener::class,
        ],
        UserSpentTokensEvent::class => [
            UpdateBalanceForUsersListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
