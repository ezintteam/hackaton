import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false;

// Bootstrap vue
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);

// Use Vue cookie to store Bearer
var VueCookie = require('vue-cookie');
Vue.use(VueCookie);

// Datepicker
import DatePicker from 'vue2-datepicker'
Vue.use(DatePicker);

// Vue Moment.js
import VueMoment from 'vue-moment'
Vue.use(VueMoment);

// Components
import '@/components'

// Axios
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Accept'] = 'application/json';

// API
import * as api from './api.js'
window.api = api;

window.ezintApi = axios.create({
  baseURL: api.apiDomain,
  timeout: 5000,
  withCredentials: true
});

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');
