import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import appRoutes from '@/views/app/router';

export default new Router({
    mode: 'history',
    linkActiveClass: 'active',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import('./views/Home.vue')
        },
        ...appRoutes,
        {
            path: '/login',
            name: 'Login',
            component: () => import('./views/Login.vue')
        },
        {
            path: '/register',
            name: 'Register',
            component: () => import('./views/Register.vue')
        },
        {
            path: '/events',
            name: 'Events',
            component: () => import('./views/Events.vue')
        },
        {
            path: '/events/:id',
            name: 'Event',
            component: () => import('./views/Event.vue')
        },
        {
            path: '/monuments',
            name: 'Monuments',
            component: () => import('./views/Monuments.vue')
        },

        { path: "*",
            component: () => import('./views/PageNotFound.vue')
        }
    ]
})
