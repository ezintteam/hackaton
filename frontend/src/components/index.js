import Vue from 'vue'

import EventForm from './EventForm';

import Navbar from './Navbar';
import Footer from './Footer';

Vue.component('event-form', EventForm);
Vue.component('navbar', Navbar);
Vue.component('footer-app', Footer);