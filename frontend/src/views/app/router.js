export default [
    {
        path: '/app',
        redirect: '/events'
    },
    {
        path: '/app/transactions',
        name: 'Transactions',
        component: () => import('./Transactions.vue')
    },
    {
        path: '/app/profile',
        name: 'Profile',
        component: () => import('./Profile.vue')
    },
];